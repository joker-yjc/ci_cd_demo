FROM node:14.21

WORKDIR /app

ADD . /app

RUN yarn

RUN npm run build

FROM nginx:stable

COPY ./build/** /usr/share/nginx/html


